#ifndef APP_H
#define APP_H


#include<iostream>
#include<vector>
#include<algorithm>
#include<cfloat>
#include <math.h>       /* round, floor, ceil, trunc */


#ifndef P
//#define P 49152.0 //mira
#define P 2048.0
#endif

long double mod(long double a, long double b);

class App
{
private:
  int id;
  //string type;
   long double release;

  //Precedence contraints 
  std::vector<App*> prec;

  //Application type
  std::string apptype;
  //Expected computation time per iteration
   long double w;
  //Requested processors
   int p;
  //Expected I/O time per iteration
   long double v;
  //Number of iteration
   int n;

  //Expected execution time
   long double eta;

  //Current state
   long double adv; //Advancement

  //Currently performing I/O
  bool active_io;

  long double b;

  //Metrics
  long double starting_date;
  long double end_date;
  long double stretch;


  

public:
  //Constructors 
  App();
  App(std::vector<std::string> param,int id,long double b);
  App(int id,  long double w,  int p,  int n,  long double v);




  //Setters & Getters 
  int getId() const;
  void setId(int id);
  
  std::string getType() const;
  void setType(std::string type);
  
   long double getW() const;
  void setW( long double w);

   int getP() const;
  void setP( int p);

  long double getV() const;
  void setV( long double v);

  long double getB() const;
  
   int getN() const;
  void setN( int p);
  
  long double getR() const;
  void setR( long double release);

  std::vector<App*> getPrec() const;
  void setPrec(std::vector<App*>);
  void AddPrec(App* app);
  void RmPrec(App* app);
  void cleanPrec();
  
  long double getEta() const;
  void setEta( int eta);

   int getIt() const;
  
   long double getAdv() const;
   void incrAdv( long double t);
   
   long double getAlpha() const;
  

   long double next();

  //Probably unused now
  bool getIOstatus() const;
  void setIO();
  void unsetIO();


  long double getStart() const;
  void setStart(long double s_d);

  long double getEnd() const;
  void setEnd(long double e_d);

  

  
};

void printApp(App *A, bool prec);
void print_applist(std::vector<App*> const &list);


bool CompRel(App* A1, App* A2) ;
bool CompTime(App* A1, App* A2);
bool CompMax(App* A1, App* A2);



/*
struct CompMax {
  CompMax(long double S) { this->S = S; }
  bool operator () (App* A1, App* A2) {
    return std::max((long double)(A1->getP())/P,A1->getAlpha()/this->S) < std::max((long double)(A2->getP())/P,A2->getAlpha()/this->S);
  }
  long double S;
};
*/
struct CompArea{
  CompArea(long double S) {this->S = S; }
  bool operator () (App* A1, App* A2) {
    if (A1->getP()*A1->getAlpha() == A2->getP()*A2->getAlpha())
      return std::max((long double)(A1->getP())/P,A1->getAlpha()/this->S) < std::max((long double)(A2->getP())/P,A2->getAlpha()/this->S);
    return (A1->getP()*A1->getAlpha() < A2->getP()*A2->getAlpha());
  }
  long double S;
};

struct CompVol{
  CompVol(long double S) {this->S = S; }
  bool operator () (App* A1, App* A2) {
    if (A1->getP()*A1->getAlpha()*A1->getEta()==A2->getP()*A2->getAlpha()*A2->getEta()){
      if (A1->getP()*A1->getAlpha() == A2->getP()*A2->getAlpha())
	return std::max((long double)(A1->getP())/P,A1->getAlpha()/this->S) < std::max((long double)(A2->getP())/P,A2->getAlpha()/this->S);
      return (A1->getP()*A1->getAlpha() < A2->getP()*A2->getAlpha());
    }
    return (A1->getP()*A1->getAlpha()*A1->getEta()<A2->getP()*A2->getAlpha()*A2->getEta());   
  }
  long double S;
};



bool CompTChar(App* A1, App* A2);
#endif



