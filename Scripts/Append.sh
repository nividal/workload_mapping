#!/bin/bash

rm compiled_results.csv

for generator in "Alpha_binormale" "Alpha_normale" "Mira_binormale" "Mira_normale"
do
    for alpha in "0.20"  "0.30"  "0.40"  "0.50"  "0.60"  "0.70"  "0.80"  "0.90"  "1.00"
    do
	for it in 0 1 2 3 4 5 6 7 8 9
	do
	    prefix="$generator \t  LS \t  0 \t  $alpha \t  $it \t "
	    awk -v prefix="$prefix" '{print prefix $0}' $generator/Workload_LS_alpha_${alpha}_n$it.dat >> compiled_results.csv
	    
	    prefix="$generator \t LS \t 0 \t $alpha \t $it \t "
	    awk -v prefix="$prefix" '{print prefix $0}' $generator/Workload_LS_mira_${alpha}_n$it.dat >> compiled_results.csv

	     prefix="$generator \t Pack \t max \t $alpha \t $it \t "
	     awk -v prefix="$prefix" '{print prefix $0}' $generator/Workload_Pack_maxtime_alpha_${alpha}_n$it.dat >> compiled_results.csv
	     prefix="$generator \t Pack \t max \t $alpha \t $it \t "
	     awk -v prefix="$prefix" '{print prefix $0}' $generator/Workload_Pack_maxtime_mira_${alpha}_n$it.dat >> compiled_results.csv
	     
	     prefix="$generator \t Pack \t char \t $alpha \t $it \t "
	     awk -v prefix="$prefix" '{print prefix $0}' $generator/Workload_Pack_chartime_alpha_${alpha}_n$it.dat >> compiled_results.csv
	     prefix="$generator \t Pack \t char \t $alpha \t $it \t "
	     awk -v prefix="$prefix" '{print prefix $0}' $generator/Workload_Pack_chartime_mira_${alpha}_n$it.dat >> compiled_results.csv


	     prefix="$generator \t Pack \t nosort \t $alpha \t $it \t "
	     awk -v prefix="$prefix" '{print prefix $0}' $generator/Workload_Pack_nosort_alpha_${alpha}_n$it.dat >> compiled_results.csv
	     prefix="$generator \t Pack \t nosort \t $alpha \t $it \t "
	     awk -v prefix="$prefix" '{print prefix $0}' $generator/Workload_Pack_nosort_mira_${alpha}_n$it.dat >> compiled_results.csv
	done
    done
done




grep -v "clock" compiled_results.csv > tmpfile && mv tmpfile compiled_results.csv
sed -i '1s/^/generator 	  paradigm 	  order 	  alpha_th 	  iteration 	 clock 	 executed 	 occupation 	 rem_ratio 	 rem_abs 	 usP 	 usB 	 t1 	 t2 	 t3 	 t4 	 n 	 n1 	 n2 	 n3 	 n4\n/' compiled_results.csv

rm compiled_app.csv

for generator in "Alpha_binormale" "Alpha_normale" "Mira_binormale" "Mira_normale"
do
    for alpha in "0.20"  "0.30"  "0.40"  "0.50"  "0.60"  "0.70"  "0.80"  "0.90"  "1.00"
    do
	for it in 0 1 2 3 4 5 6 7 8 9
	do
	    prefix="$generator \t  LS \t  0 \t  $alpha \t  $it \t "
	    awk -v prefix="$prefix" '{print prefix $0}' $generator/Application_LS_alpha_${alpha}_n$it.dat >> compiled_app.csv
	    
	    prefix="$generator \t LS \t 0 \t $alpha \t $it \t "
	    awk -v prefix="$prefix" '{print prefix $0}' $generator/Application_LS_mira_${alpha}_n$it.dat >> compiled_app.csv

	     prefix="$generator \t Pack \t max \t $alpha \t $it \t "
	     awk -v prefix="$prefix" '{print prefix $0}' $generator/Application_Pack_maxtime_alpha_${alpha}_n$it.dat >> compiled_app.csv
	     prefix="$generator \t Pack \t max \t $alpha \t $it \t "
	     awk -v prefix="$prefix" '{print prefix $0}' $generator/Application_Pack_maxtime_mira_${alpha}_n$it.dat >> compiled_app.csv
	     
	     prefix="$generator \t Pack \t char \t $alpha \t $it \t "
	     awk -v prefix="$prefix" '{print prefix $0}' $generator/Application_Pack_chartime_alpha_${alpha}_n$it.dat >> compiled_app.csv
	     prefix="$generator \t Pack \t char \t $alpha \t $it \t "
	     awk -v prefix="$prefix" '{print prefix $0}' $generator/Application_Pack_chartime_mira_${alpha}_n$it.dat >> compiled_app.csv 


	     	     prefix="$generator \t Pack \t nosort \t $alpha \t $it \t "
	     awk -v prefix="$prefix" '{print prefix $0}' $generator/Application_Pack_nosort_alpha_${alpha}_n$it.dat >> compiled_app.csv
	     prefix="$generator \t Pack \t nosort \t $alpha \t $it \t "
	     awk -v prefix="$prefix" '{print prefix $0}' $generator/Application_Pack_nosort_mira_${alpha}_n$it.dat >> compiled_app.csv 
	done
    done
done

grep -v "SchedPol" compiled_app.csv > tmpfile && mv tmpfile compiled_app.csv
sed -i '1s/^/generator \t paradigm \t order \t alpha_th \t iteration \t SchedPol \t ContPol \t Em \t S \t alpha \t  p \t w \t v \t n \t start \t end \t adv \t eta\n/' compiled_app.csv
