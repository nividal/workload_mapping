#!/bin/bash

csplit -z -f 'temp' -b '%02d.csv' occupation.csv /Workload_/ {*}

for file in `ls temp*`
do
    line=$(head -1 $file)
    sed '1d' $file >$line
    rm $file
done

csplit -z -f 'temp' -b '%02d.csv' application_metrics.csv /Application_/ {*}
for file in `ls temp*`
do
    line=$(head -1 $file)
    sed '1d' $file >$line
    rm $file
done

for file in `ls Application_LS*`
do
    csplit -z -f 'temp' -b '%02d.csv' $file /SchedPol/ {*}
    mv temp00.csv $file
    mv temp01.csv ${file/LS/Pack_nosort}
done


for file in `ls Workload_LS*`
do
    csplit -z -f 'temp' -b '%02d.csv' $file /clock/ {*}
    mv temp00.csv $file
    mv temp01.csv ${file/LS/Pack_nosort}
done
