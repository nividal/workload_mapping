#ifndef PLATFORM_H // include guard
#define PLATFORM_H


#include<vector>
#include<algorithm>
#include <cmath>        // std::std::abs
#include<map>
#include<set>
#include<utility>
#include "App.h"
#include "Parameters.h"
#include "scheduler.h"


#ifndef P
//#define P 49152.0 //Mira
#define P 2048
#endif



class Platform{
private:
  std::vector<App*> release_queue; //A trier ça ne coute rien
  std::vector<App*> precedence_queue;
  std::vector<App*> waiting_queue;
  std::vector<App*> compute_queue; 
  std::vector<App*> io_queue;
  std::vector<App*> current_io;
  std::vector<App*> completed;

  std::vector<App*> comp_buffer;
  std::vector<App*> io_buffer;

  long double mean_job;
  
  
  int usP;
  long double usB;

  long double Tmax;

  bool pocc;
  bool bckfllng;
  long double clock;
  
  long double total_work;
  
public:
  Platform(std::vector<App*> *applist, Parameters *param);
  int init(std::vector<App*> *applist, Parameters *param);
  bool isOver();
  long double next(Parameters *param);
  int resolve_comp( long double next_event);
  int resolve_release( long double next_event, Parameters *param);
  int resolve_io( long double next_event, Parameters *param);

  int print_status();
  int print_metrics(Parameters *param,std::ofstream& cam, std::ofstream& cem);
  int print_occupation(Parameters *param, std::ofstream & cocc);
  long double getClock() const;

  bool getPocc() const;
  int setPocc();
  int unsetPocc();

  void incrClock(long double t);
  
  //int check_queues(Parameters *param, bool stable=false);
  int place(Parameters* param);

  int resolve_buffer();
  bool resource_constraint(App* App, Parameters *param);

 int backfilling(Parameters *param);
 int reSchedule(Parameters *param);
 bool enableback();
 
 

  
};


int print_vector(std::vector<App*> *vect);



#endif
