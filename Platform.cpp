#include"Platform.h"

Platform::Platform(std::vector<App*> *applist, Parameters *param){
  this->usP=0;
  this->usB=0;
  this->clock=0;
  this->Tmax=0;
  this->release_queue.clear();
  this->precedence_queue.clear();
  this->waiting_queue.clear();
  this->compute_queue.clear();
  this->io_queue.clear();
  this->current_io.clear();
  this->io_buffer.clear();
  this->comp_buffer.clear();
  this->completed.clear();
  this->total_work=0;
  this->pocc=false;
  this->bckfllng=false;
  this->mean_job=0;
  

  //gantt this->gantt.clear();
  //Initialized with clean vectors
  
  for (std::vector<App*>::iterator it = applist->begin(); it != applist->end(); ++it){
    this->total_work+= ((*it)->getP())*((*it)->getEta());//bugged ?
    this->mean_job += (*it)->getEta();
    if (!( (*it)->getPrec().empty()))
      this->precedence_queue.push_back(*it);
    else if  ((*it)->getR()<=0)
      {
	if ((resource_constraint(*it, param)))
	  {
	    (*it)->setStart(this->clock);
	    this->compute_queue.push_back(*it);
	    this->usP+=(*it)->getP();
	    this->usB+=(*it)->getAlpha();
	    this->Tmax=std::max(this->Tmax,(*it)->getEta());
	    this->setPocc();
	    this->bckfllng=true;
	    //gantt this->gantt[ (*it)->getId() ].push_back(std::make_pair('s', this->clock));
	  }
	else
	  this->waiting_queue.push_back(*it);
      }
    else if ((*it)->getR()>0)
      this->release_queue.push_back(*it);
  }
  this->mean_job =mean_job/applist->size();
  std::sort((this->release_queue).begin(),(this->release_queue).end(), CompRel);
  /*
    std::cout<<"Platform initialised with ";
    std::cout<<this->release_queue.size()+ this->precedence_queue.size()+ this->waiting_queue.size() + this->compute_queue.size() + this->io_queue.size()+this->current_io.size();
    std::cout<<" applications"<<std::endl;
  */
}

// Place applist into correct queues
int Platform::init(std::vector<App*> *applist, Parameters *param){
    for (std::vector<App*>::iterator it = applist->begin(); it != applist->end(); ++it){
      this->total_work+= ((*it)->getP())*((*it)->getEta());//bugged ?
      this->mean_job += (*it)->getEta();
      if (!( (*it)->getPrec().empty()))
	this->precedence_queue.push_back(*it);
      else if  ((*it)->getR()<=0)
	{
	  if ((resource_constraint(*it, param)))
	    {
	      (*it)->setStart(this->clock);
	      this->compute_queue.push_back(*it);
	      this->usP+=(*it)->getP();
	      this->usB+=(*it)->getAlpha();
	      this->Tmax=std::max(this->Tmax,(*it)->getEta());
	      this->setPocc();
	      this->bckfllng=true;
	      //gantt this->gantt[ (*it)->getId() ].push_back(std::make_pair('s', this->clock));
	    }
	  else
	    this->waiting_queue.push_back(*it);
	}
      else if ((*it)->getR()>0)
	this->release_queue.push_back(*it);
    }
    this->mean_job =mean_job/applist->size();
    std::sort((this->release_queue).begin(),(this->release_queue).end(), CompRel);
    return 0;
}





bool Platform::getPocc() const{
  return this->pocc;
}

int Platform::setPocc(){
  this->bckfllng=true;
  this->pocc=true;
  return 0;
}

int Platform::unsetPocc(){
  this->pocc=false;
  return 0;
}



//Tools
bool Platform::isOver(){
  return (this->release_queue.empty() && this->compute_queue.empty() && this->io_queue.empty() && this->precedence_queue.empty() && this->waiting_queue.empty() && this->current_io.empty() );
}


int Platform::place(Parameters* param){
  //Depending on parameters
  if (this->compute_queue.empty() && this->io_queue.empty() && this->current_io.empty()){

    //Useless.
    switch ( param->getEmPol() ){
    case 0:
      std::sort(this->waiting_queue.begin(), this->waiting_queue.end(),CompMax);
      break;
    case 1:
      std::sort(this->waiting_queue.begin(), this->waiting_queue.end(),CompArea(param->getS()));
      break;
    case 2:
      std::sort(this->waiting_queue.begin(), this->waiting_queue.end(),CompVol(param->getS()));
      break;
    default:
      break;
    }
    auto it = this->waiting_queue.begin();
    this->compute_queue.push_back(*it);
    //gantt this->gantt[ (*it)->getId() ].push_back(std::make_pair('c',this->clock));
    this->waiting_queue.erase(it);
  }

  if ((this->current_io.empty())&&(!this->io_queue.empty()))
    {
      this->current_io.push_back(this->io_queue[0]);
      //gantt this->gantt[ (*it)->getId() ].push_back(std::make_pair('i',this->clock));
      this->io_queue.erase(this->io_queue.begin());
    }
  return 0;
}

//Time until next event
long double Platform::next(Parameters *param){
  long double next_event=DBL_MAX;
  long double B=param->getB();
  if (!this->release_queue.empty())
    if (this->release_queue[0]->getR()-this->clock < next_event)
      next_event=this->release_queue[0]->getR()-this->clock;


  for (std::vector<App*>::iterator cpit=this->compute_queue.begin(); cpit != this->compute_queue.end(); ++cpit){
    if ( (*cpit)->next() < next_event)
      next_event=(*cpit)->next();
  }
  if (!this->current_io.empty()){
    switch (param->getEmPol()){     
    default:
      if (this->current_io[0]->next() < next_event)
	next_event=this->current_io[0]->next();
      break;   
    }  
  }
  return next_event;
}


//Resolve functions
// IO buffer 
int Platform::resolve_comp( long double next_event){
  double it_next;
  auto it=this->compute_queue.begin();
  while (it !=this->compute_queue.end() )
    {
      it_next=(*it)->next();
      (*it)->incrAdv(next_event); 
      if ((*it)->getAdv()> (*it)->getEta())
	std::cerr<<(*it)->getId()<<" : error: this application should have finished"<<std::endl;


      if (( std::abs(next_event - it_next )  < 0.0001) || (mod((*it)->getAdv(),(*it)->getV()+(*it)->getW()) >(*it)->getW() )) 
	{
	  this->io_buffer.push_back(*it);
	  it=this->compute_queue.erase(it);
	}
      else
	{
	  it++;
	}
    }
  return 0;
}
//Waiting queue ?
int Platform::resolve_release( long double next_event, Parameters *param){
  if (this->release_queue.empty())
    return 0;
  long double clock = this-> clock;
  
  while (this->release_queue[0]->getR() <= clock+next_event)
    {     
      if (resource_constraint(this->release_queue[0], param))
	{
	  this->release_queue[0]->setStart(this->clock+next_event);
	  //gantt this->gantt[ this->release_queue[0]->getId() ].push_back(std::make_pair('s',this->clock));
	  this->comp_buffer.push_back(this->release_queue[0]);
	  this->usP+=release_queue[0]->getP();
	  this->usB+=release_queue[0]->getAlpha();
	  this->Tmax=std::max(Tmax,this->release_queue[0]->getR()+this->release_queue[0]->getEta());
	  this->setPocc();
	}
      else
	this->waiting_queue.push_back(this->release_queue[0]);
      this->release_queue.erase(this->release_queue.begin()); 
    }


  return 0;
}




int Platform::resolve_io( long double next_event, Parameters *param){
  bool space;
  double it_next;
  long double B=param->getB();
  if (this->current_io.empty())
    {
      return 0;
    }
  long double prog=B*next_event/(this->current_io.size());
  auto it=this->current_io.begin();
  space=false;
  while(it!=this->current_io.end() )
    {
      it_next=(*it)->next();
      (*it)->incrAdv(prog);
  
      //Application is over
      bool end_exec= (  abs( ((*it)->getAdv()) - (*it)->getEta()  ) < 0.0001 ) || (((*it)->getAdv()) >= (*it)->getEta()) ;   //Condition: End of application
      bool end_phase= ( std::abs( it_next - prog/B)  < 0.0001); //Condition: End of IO phase
      

      if (end_exec || end_phase) {
        if ( end_exec  ){
	  (*it)->setEnd(this->clock+next_event);
	  this->usP-=(*it)->getP();
	  this->usB-=(*it)->getAlpha();
	  this->completed.push_back(*it);
	  //gantt this->gantt[ (*it)->getId() ].push_back(std::make_pair('e',this->clock));
	  this->setPocc();
	  auto pit=this->precedence_queue.begin();
	  while (pit != this->precedence_queue.end()){
	    (*pit)->RmPrec(*it);
	    if ((*pit)->getPrec().empty())
	      {
		if ( (*pit)->getR()+next_event <= this->clock)
		  this->waiting_queue.push_back(*pit);
		else
		  this->release_queue.push_back(*pit);
		pit=precedence_queue.erase(pit);
	      }
	    else
	      ++pit;
	  }
	  it=this->current_io.erase(it);
	  space=true;

	}
	else  
	  {
	    this->comp_buffer.push_back(*it);
	    it=this->current_io.erase(it);
	  }
      }
      else 
	{
	  ++it;
	}
    }
  
  if ((space)&&(!this->waiting_queue.empty())) {

    //useless
    switch ( param->getEmPol() ){
    case 0:
      std::sort(this->waiting_queue.begin(), this->waiting_queue.end(),CompMax);
      break;
    case 1:
      std::sort(this->waiting_queue.begin(), this->waiting_queue.end(),CompArea(param->getS()));
      break;
    case 2:
      std::sort(this->waiting_queue.begin(), this->waiting_queue.end(),CompVol(param->getS()));
      break;
    default:
      break;
    }
    int tg;
  
    do {
      tg=-1;
      for (unsigned int jt=0;jt!=this->waiting_queue.size();++jt){
	if ( resource_constraint (this->waiting_queue[jt] ,param)){
	  tg=jt;
	  break;
	}
      }
      if (tg>-1){
	this->waiting_queue[tg]->setStart(this->clock+next_event);
	//gantt this->gantt[ this->waiting_queue[tg]->getId() ].push_back(std::make_pair('s',this->clock));
	this->comp_buffer.push_back(this->waiting_queue[tg]);
	this->usP+=(this->waiting_queue[tg]->getP());
	this->usB+=(this->waiting_queue[tg]->getAlpha());
	this->Tmax=std::max(Tmax,this->clock+next_event+this->waiting_queue[tg]->getEta());
	this->waiting_queue.erase(this->waiting_queue.begin()+tg);
	this->setPocc();
      }
    }

    while (tg != -1 );
    std::cerr<<"resolve_io_fin boucle 3\n";
  }
  return 0;   
}

int Platform::print_status(){
  //std::cout<<"t \t"<<this->clock<<"\t"<<P-usP<<"\t"<<usB<<std::endl;
  /*
    std::cout<<"##################################"<<std::endl;
    std::cout<<"clock: "<<this->clock<<std::endl;
    std::cout<<"usP: "<<this->usP<<std::endl;
    std::cout<<"usB: "<<this->usB<<std::endl;
    //std::cout<<"precedence: ";
    //print_vector(&(this->precedence_queue));
    //std::cout<<std::endl;
    //std::cout<<"release: ";
    //print_vector(&(this->release_queue));
    //std::cout<<std::endl;
    //std::cout<<"waiting: ";
    //print_vector(&(this->waiting_queue));
    //std::cout<<std::endl;
    std::cout<<"compute: ";
    print_vector(&(this->compute_queue));
    std::cout<<std::endl;
    std::cout<<"ioqueue: ";
    print_vector(&(this->io_queue));
    std::cout<<std::endl;
    std::cout<<"currentio: ";
    print_vector(&(this->current_io));
    std::cout<<std::endl;

    std::cout<<"Queues status prec:"<< this->precedence_queue.size();
    std::cout<<" release:"<<this->release_queue.size();
    std::cout<<" waiting:"<<this->waiting_queue.size();
    std::cout<<" compute:"<<this->compute_queue.size();
    std::cout<<" ioqueue:"<<this->io_queue.size();
    std::cout<<" currentio:"<<this->current_io.size();
    std::cout<<" completed:"<<this->completed.size() <<std::endl;
  */

  return 0;    
}


int Platform::print_occupation(Parameters *param, std::ofstream & cocc){
  long double completed=0;
  long double remaining_abs;
  long double remaining_ratio;

  int n_app;
  n_app=this->completed.size()+this->io_queue.size()+this->compute_queue.size()+this->current_io.size();

  int n1=this->completed.size();
  int n2=this->io_queue.size();
  int n3=this->compute_queue.size();
  int n4 =this->current_io.size();

  //Ideally move completed to make it a platfrom attribute (like total_work)
  
  long double t1=0;
  for (std::vector<App*>::iterator it=this->completed.begin();it!=this->completed.end();it++)
    t1+= ((*it)->getP()*(*it)->getAdv());


  long double t2=0;
  for (std::vector<App*>::iterator it=this->io_queue.begin();it!=this->io_queue.end();it++)
    t2+= ((*it)->getP()*(*it)->getAdv());


  long double t3=0;
  for (std::vector<App*>::iterator it=this->compute_queue.begin();it!=this->compute_queue.end();it++)
    t3+= ((*it)->getP()*(*it)->getAdv());

  long double t4=0;
  for (std::vector<App*>::iterator it=this->current_io.begin();it!=this->current_io.end();it++)
    t4+= ((*it)->getP()*(*it)->getAdv());


  completed=t1+t2+t3+t4;
  remaining_abs=this->total_work-completed;
  remaining_ratio=remaining_abs/this->total_work;
  cocc<<this->clock<<"\t"<<completed<<"\t"<<completed/(P*this->clock)<<"\t"<<remaining_ratio<<"\t";
  cocc<<remaining_abs/P<<"\t"<<this->usP<<"\t"<<this->usB<<"\t"<<t1<<"\t"<< t2<<"\t"<<t3<<"\t"<<t4<<"\t"<<n_app<<"\t"<<n1<<"\t"<< n2<<"\t"<<n3<<"\t"<<n4<<std::endl;
  std::cout<<"completed: "<<completed<<"vs total_work:"<<this->total_work<<" remaining_abs: "<<remaining_abs<<" remaining_ratio:"<<remaining_ratio<<std::endl;
  //mean_job -> u.t / hours
  return 0;
}




int Platform::print_metrics(Parameters *param, std::ofstream& cam, std::ofstream& cem){
  //Append to file
  long double makespan=0;
  long double mean_stretch=0;
  long double max_stretch = 0;
  
  long double vi=0;
  long double pi_wi_vi=0;
  
  long double total_compute=0;
  long double total_io=0;
  long double total_idle=0;
  long double total_cont=0;

  for (std::vector<App*>::iterator it=this->completed.begin();it!=this->completed.end();it++){
    //std::cout<<"App Id:"<<(*it)->getId()<<std::endl;
    //std::cout<<"start: "<<(*it)->getStart()<<" \t "<<"end: "<<(*it)->getEnd()<<std::endl;
    //std::cout<<"compute: "<<((*it)->getW())*((*it)->getN())<<" \t io: "<<((*it)->getV())*((*it)->getN())<<" \t total: "<<(*it)->getEta()<<std::endl;
    //std::cout<<"w: "<<(*it)->getW()<<" \t v: "<<(*it)->getV()<<" \t n: "<<(*it)->getN()<<std::endl;
    makespan=std::max(makespan,(*it)->getEnd());
    mean_stretch += ((*it)->getEnd()-(*it)->getStart())/((*it)->getEta());
    max_stretch=std::max(max_stretch,((*it)->getEnd()-(*it)->getStart() )/((*it)->getEta()));


    vi+=(*it)->getV()*((*it)->getN());
    pi_wi_vi+=((*it)->getN())*((*it)->getP())*((*it)->getW()+(*it)->getV()) ;

    total_compute+= ((*it)->getN())*((*it)->getP()*(*it)->getW());
    total_io += (  (*it)->getP()*  ((*it)->getN())*((*it)->getV()/param->getB())); //Add *p compared to final results
    total_cont += (*it)->getP()*(((*it)->getEnd()-(*it)->getStart() )-((*it)->getEta()));  //In final results total cont is false (P.MS-OPT) au lieu de P(MS-OPT)
  }

  std::cerr<<"P="<<P<<std::endl;
  total_idle=makespan*P-(total_compute+total_io+total_cont); //In final results total idle is therefore false
  mean_stretch /= this->completed.size();
  /*
    std::cout<<this->completed.size()<<" applications completed in "<<this->clock<<" u.t."<<std::endl;
    std::cout<<"alpha: "<<pi_vi/pi_wi_vi<<std::endl;
    std::cout<<"makespan: "<<makespan<<std::endl;
    std::cout<<"max_stretch: "<<max_stretch<<" \t mean_stretch: "<<mean_stretch<<std::endl;
  */
  
  
  for (auto jt=this->completed.begin();jt!=this->completed.end();jt++){
    cam<<param->getSchedPol()<<" \t "<<param->getContPol()<<" \t "<<param->getEmPol()<<" \t "<<param->getS()<<" \t ";
    cam<<(*jt)->getAlpha()<<" \t ";
    cam<<(*jt)->getP()<<" \t "<<(*jt)->getW()<<" \t "<<(*jt)->getV()<<" \t "<<(*jt)->getN()<<" \t ";
    cam<<(*jt)->getStart()<<" \t "<<(*jt)->getEnd()<<"\t"<<(*jt)->getAdv()<<"\t"<<(*jt)->getEta()<<std::endl;
  }

  cem<<param->getSchedPol()<<" \t "<<param->getContPol()<<" \t "<<param->getEmPol()<<" \t "<<param->getS()<<" \t ";
  cem<<P*vi/pi_wi_vi<<" \t "<<param->getB()<<" \t ";
  cem<<makespan<<" \t "<<max_stretch<<" \t "<<mean_stretch<<" \t ";
  cem<<" \t "<<total_compute<<" \t "<<total_io<<" \t "<<total_cont<<" \t "<<total_idle<<" \t "<<vi<<std::endl;
  return 0;
}


long double Platform::getClock() const {return this->clock;}
void Platform::incrClock(long double t){this->clock+=t;}


int Platform::resolve_buffer(){
  for (auto it = this->io_buffer.begin() ; it != this->io_buffer.end();it++ ){
    (*it)->setIO();
    //gantt this->gantt[ (*it)->getId() ].push_back(std::make_pair('q',this->clock));
    this->io_queue.push_back(*it);
  }
  
  for (auto jt = this->comp_buffer.begin() ; jt != this->comp_buffer.end();jt++ ){

    (*jt)->unsetIO();
    //gantt this->gantt[ (*jt)->getId() ].push_back(std::make_pair('c',this->clock));
    this->compute_queue.push_back(*jt);
  }
  this->io_buffer.clear();
  this->comp_buffer.clear();
  return 0;
}


bool Platform::resource_constraint(App* App, Parameters *param){
  switch (param->getSchedPol() ) {
  case 0: case 1: case 2: case 3:
    return (App->getP() <= P-this->usP) ;
    break;    
  case 4: default:
    {
      if (this->waiting_queue.empty()){
	return ( App->getP() <= P-this->usP ) && (App->getAlpha()  <= param->getS()-this->usB );
      }
      else if  (App->getId() == (*(this->waiting_queue.begin() ) ) ->getId()){
	return ( App->getP() <= P-this->usP ) && (App->getAlpha()  <= param->getS()-this->usB );
      }
      return false;
    }
    break;
  }
}

int print_vector(std::vector<App*> *vect){
  std::cout<<"[ ";
  
  for (std::vector<App*>::iterator it=vect->begin(); it!= vect->end();++it){
    if (it != vect->begin() )
      std::cout<<" , ";
    std::cout<<(*it)->getId();


  }
  std::cout<<" ]   size:"<<vect->size();
  
  return 0;
}


int Platform::backfilling(Parameters *param){
  double eta;
  long int firstp;
  long double firstb;
  long double remaining_io=0;
  bool bprec;
  std::map<long double, std::pair<int, long double > > expected_space;
  long double tend;
  bool search;
  expected_space.clear();

  expected_space[0]=std::make_pair(P-this->usP, param->getB()-this->usB);

  
  // ### Update following compute queue  ###
  std::cout<<"### Update following compute queue  ###"<<std::endl;
  for (std::vector<App*>::iterator it=this->compute_queue.begin(); it!= this->compute_queue.end();++it){
    eta=(*it)->getEta()-(*it)->getAdv();
    firstp = -1;
    firstb= -1;

    for (std::map<long double, std::pair<int, long double > >::iterator et = expected_space.begin() ; et != expected_space.end(); ++et ){
      if ((et->first)>=eta){
	(et->second).first += (*it)->getP();
	(et->second).second += (*it)->getAlpha();
	if (firstp=-1){
	  firstp=(et->second).first;
	  firstb=(et->second).second;
	}
      }
    }
    if (firstp=-1){
      firstp= (expected_space.rbegin()->second).first + (*it)->getP();
      firstb= (expected_space.rbegin()->second).second + (*it)->getAlpha();
    }
    expected_space[eta]=std::make_pair(firstp,firstb);
  }


  // ### Update following io queue  ###
  std::cout<<"### Update following io queue  ###"<<std::endl;
  for (std::vector<App*>::iterator it=this->io_queue.begin(); it!= this->io_queue.end();++it){
    eta=(*it)->getEta()-(*it)->getAdv();

    firstp = -1;
    firstb= -1;

    for (std::map<long double, std::pair<int, long double > >::iterator et = expected_space.begin() ; et != expected_space.end(); ++et ){
      if ((et->first)>=eta){
	(et->second).first += (*it)->getP();
	(et->second).second += (*it)->getAlpha();
	if (firstp=-1){
	  firstp=(et->second).first;
	  firstb=(et->second).second;
	}
      }
    }
    if (firstp=-1){
      firstp=(expected_space.rbegin()->second).first + (*it)->getP();
      firstb=(expected_space.rbegin()->second).second + (*it)->getAlpha();
    }
    expected_space[eta]=std::make_pair(firstp,firstb);
  }

  // ### Update following current io  queue  ###
  std::cout<<"### Update following current io  queue  ###"<<std::endl;
  for (std::vector<App*>::iterator it=this->current_io.begin(); it!= this->current_io.end();++it){
    eta=(*it)->getEta()-(*it)->getAdv();

    firstp = -1;
    firstb= -1;

    for (std::map<long double, std::pair<int, long double > >::iterator et = expected_space.begin(); et != expected_space.end(); ++et ){
      if ((et->first)>=eta){
	(et->second).first += (*it)->getP();
	(et->second).second += (*it)->getAlpha();
	if (firstp=-1){
	  firstp=(et->second).first;
	  firstb=(et->second).second;
	}
      }
    }

    if (firstp=-1){
      firstp=(expected_space.rbegin()->second).first += (*it)->getP();
      firstb=(expected_space.rbegin()->second).second += (*it)->getAlpha();
    }
    expected_space[eta]=std::make_pair(firstp,firstb);
  }
    
  // ### Predict future packs ###


  long double upperbound = expected_space.rbegin()->first;
  std::set<int> seen;
  seen.clear();
  std::cout<<" ### Predict future packs ###"<<std::endl;
  
  for (std::vector<App*>::iterator it= this->completed.begin();it!=this->completed.end();it++){
    seen.insert((*it)->getId());
  } 
  
  for (std::vector<App*>::iterator it= this->compute_queue.begin();it!=this->compute_queue.end();it++){
    seen.insert((*it)->getId());
  } 
  for (std::vector<App*>::iterator it= this->io_queue.begin();it!=this->io_queue.end();it++){
    seen.insert((*it)->getId());
  } 
  for (std::vector<App*>::iterator it= this->current_io.begin();it!=this->current_io.end();it++){
    seen.insert((*it)->getId());
  }
  for (std::vector<App*>::iterator it= this->waiting_queue.begin();it!=this->waiting_queue.end();it++){
    seen.insert((*it)->getId());
  } 
  
  while (seen.size() < (this->completed.size()+this->compute_queue.size()+this->io_queue.size()+this->current_io.size()+this->precedence_queue.size()+this->waiting_queue.size()))   {
    tend=expected_space.rbegin()->first;
    std::cout<<"applications seen:"<<seen.size()<< "over "<< (this->completed.size()+this->compute_queue.size()+this->io_queue.size()+this->current_io.size()+this->precedence_queue.size()+this->waiting_queue.size())<<std::endl;
    for (std::vector<App*>::iterator it=this->precedence_queue.begin();it != this->precedence_queue.end(); it++) {
      if (seen.count((*it)->getId())==0 ) {
        bprec=true;
        if (!((*it)->getPrec()).empty()   ){
          std::vector<App*> lprec = (*it)->getPrec(); //local copy
          for (std::vector<App*>::iterator jt=lprec.begin(); jt!=lprec.end();++jt){
	    bprec &= (seen.count((*jt)->getId())>0);
          }
        }
	if (bprec)
	  {
	    eta=tend+(*it)->getEta();
	    for (std::map<long double, std::pair<int,long double> >::iterator et = expected_space.begin(); et != expected_space.end(); et++){
	      if (((et->first>=tend)&&((et->first)<eta))){
		(et->second).first  -= (*it)->getP();
		(et->second).second -= (*it)->getAlpha();
	      }
	    }
	    if (eta > expected_space.rbegin()->first){
	      expected_space[eta]=std::make_pair(P,param->getB());
	    }
	    seen.insert((*it)->getId());
	  }
      }
    }
  }
  // Prediction for backfilling with waiting and precedence queue (in this order)

  std::cout<<"predict waiting applications"<<std::endl;
  std::vector<App*>::iterator it= this->waiting_queue.begin();
  while(it != waiting_queue.end()){

    int incr=1;
    std::map<long double, std::pair<int,long double> >::iterator et = expected_space.begin();
    search=true;
    
    while(search){
      while ( ( (et->second).first < (*it)->getP() || (et->second).second < (*it)->getAlpha() )  ||     ( (et->first<upperbound) && (et->first + (*it)->getEta() > upperbound)  ) ){  
        std::cout<<"test begin: "<<et->first<<std::endl;
        et++;
      }
    
      eta = et->first + (*it)->getEta();
      std::map<long double, std::pair<int,long double> >::iterator ed =et;
      
      while ( (ed->second).first <= (*it)->getP() && (ed->second).second <= (*it)->getAlpha() && (ed->first<eta)){
        
        ed++;
      }

      //If reach the end of schedule or application -> fits // If not we go past this space to find another.
      if((ed==expected_space.end()) || (ed->first>eta ))
	search=false;
      else if (et != ed)
	et=ed;
      else
	et++;
      search &= (et!=expected_space.end());
    }


    if (et == expected_space.end()){
      std::cerr<<"Backfilling error ; use resources never get released"<<std::endl;
    }

    if (et == expected_space.begin()){


      this->usP+=(*it)->getP();
      this->usB+=(*it)->getB();
      //gantt this->gantt[ (*it)->getId() ].push_back(std::make_pair('s',this->clock));
      //gantt this->gantt[ (*it)->getId() ].push_back(std::make_pair('c',this->clock));
      (*it)->setStart(this->clock);
      incr=0;
      this->compute_queue.push_back(*it);
		  
    }
    eta = et->first + (*it)->getEta();
		
    //maj 
    while((et->first < eta)  && (et != expected_space.end()) ){
      (et->second).first -= (*it)->getP();
      (et->second).second -= (*it)->getAlpha();
      et++;
    }

    //Application end
    if (et ==expected_space.end()){
      expected_space[eta]=std::make_pair(P,param->getB());
    }
    else{
      et--;
      firstp=et->second.first;
      firstb=et->second.second;
      expected_space[eta]=std::make_pair(firstp+(*it)->getP(),firstb+(*it)->getAlpha());
      
    }

    if (incr)
      it ++;
    else{
      it=this->waiting_queue.erase(it);
      this->reSchedule(param);
    }
  }


  std::cout<<"predict applications with dependancies"<<std::endl;
  it= this->precedence_queue.begin();

  while (it != this->precedence_queue.end()){
    int incr=1;
    std::map<long double, std::pair<int,long double> >::iterator et = expected_space.begin();
    search=true;
    
    while(search){

      
      while ( ( (et->second).first < (*it)->getP() || (et->second).second < (*it)->getAlpha() )  ||     ( (et->first<upperbound) && (et->first + (*it)->getEta() > upperbound)  ) ){  
        et++;
      }
      
      eta = et->first + (*it)->getEta();
      std::map<long double, std::pair<int,long double> >::iterator ed =et;
      
      while ( (ed->second).first <= (*it)->getP() && (ed->second).second <= (*it)->getAlpha() && (ed->first<eta)){
        
        ed++;
      }
      std::cout<<"iterator state et:"<<et->first<<"    ed:"<<ed->first<<std::endl;
      //If reach the end of schedule or application -> fits // If not we go past this space to find another.
      if((ed==expected_space.end()) || (ed->first>eta ))
	search=false;
      else if (et != ed)
	et=ed;
      else
	et++;
      search &= (et!=expected_space.end());
    }
    

      

    if (et == expected_space.end()){
      //std::cerr<<"Backfilling error ; used resources never get released"<<std::endl;
    }

    if (et == expected_space.begin()){
      this->usP+=(*it)->getP();
      this->usB+=(*it)->getB();
      (*it)->setStart(this->clock);
      //gantt this->gantt[ (*it)->getId() ].push_back(std::make_pair('s',this->clock));
      this->compute_queue.push_back(*it);
      //gantt this->gantt[ (*it)->getId() ].push_back(std::make_pair('c',this->clock));
      incr=0;
    }

    eta = et->first + (*it)->getEta();
		
    //maj 
    while(et->first < eta  && et != expected_space.end()){
      (et->second).first -= (*it)->getP();
      (et->second).second -= (*it)->getAlpha();
      et++;
    }

    //Application end
    if (et ==expected_space.end()){
      expected_space[eta]=std::make_pair(P,param->getB());
    }
    else{
      et--;
      firstp=et->second.first;
      firstb=et->second.second;
      expected_space[eta]=std::make_pair(firstp+(*it)->getP(),firstb+(*it)->getAlpha());
    }

    if (incr)
      it += incr;
    else
      {
	it=this->precedence_queue.erase(it);
	this->reSchedule(param);
      }
  }
  
  this->bckfllng=false;
  return 0;
}


int Platform::reSchedule(Parameters *param){
  std::vector<App*> applist;
  applist.clear();
  applist.insert(applist.end(),this->waiting_queue.begin(),this->waiting_queue.end());
  applist.insert(applist.end(),this->release_queue.begin(),this->release_queue.end());
  applist.insert(applist.end(),this->precedence_queue.begin(),this->precedence_queue.end());
  
  scheduler(&applist, param);
  this->init(&applist, param);
  return 0;
}



bool Platform::enableback(){
  // No backfilling if the platform is empty (to avoid taking priority over packs)
  if (this->compute_queue.empty() && this->current_io.empty() && this->io_queue.empty() )
    return false;
  return this->bckfllng;}
