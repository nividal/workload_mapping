#include "emulator.h"



int resolve(Platform *platform,Parameters *param,  long double next_event){

  std::cerr<<"Resolve computation"<<std::endl;
  if (platform->enableback()){
  platform->backfilling(param);
}

  platform->print_status();
  platform->resolve_comp(next_event);
  std::cerr<<"Resolve releases"<<std::endl;
  platform->print_status();
  platform->resolve_release(next_event,param);
  std::cerr<<"Resolve I/O operations"<<std::endl;
  platform->print_status();
  platform->resolve_io(next_event,param);
  std::cerr<<"Resolve buffers"<<std::endl;
  platform->print_status();
  platform->resolve_buffer();
  std::cerr<<"Resolution completed"<<std::endl;
  return 0;
}
