#include "scheduler.h"

int packs_to_list(std::vector< std::vector<App*> > ev,std::vector<App*> *applist){
  std::vector<App*> prec;
  prec.clear();
  applist->clear();
  
  for (std::vector< std::vector<App*> >::iterator itev=ev.begin(); itev != ev.end(); ++itev){
    std::vector<App*> currprec = prec; //copy

    for (std::vector<App*>::iterator itapp=itev->begin(); itapp != itev->end(); ++itapp){
      (*itapp)->setPrec(currprec);
      prec.push_back(*itapp);
      applist->push_back(*itapp);
    }  
  }
  return 0;
}


int pack_depth(std::vector<App*> *applist, Parameters *param){
  std::cerr<<"Deep pack scheduling"<<std::endl;
  std::vector< std::vector<App*> > ev; 
  std::vector<long double> us_v ;
  std::vector<int> us_p ;
  std::vector<long double> tmax; 
  
  us_v.clear();
  us_p.clear();
  tmax.clear();

  
  /*
  print_applist(*applist);
  //OK
  */
  for (std::vector<App*>::iterator it=applist->begin();it!=applist->end();++it){
    (*it)->cleanPrec();
  }
  

  
  switch ( param->getSchedPol() ){
  case 0:
    std::sort(applist->begin(),applist->end(),CompTime);
    break;
  case 2:
    std::sort(applist->begin(),applist->end(),CompTChar);
    break;
  case 6:
    std::sort(applist->begin(),applist->end(),CompMax);
    break;
  }
  
  //sort applications according to sorting policy for packs building?

  
  auto itapp = applist->begin();
  while (itapp != applist->end()) {

    unsigned int itev = 0;
    while ( itev != ev.size() ) {
      if ( (((*itapp)->getV()*(*itapp)->getN()) <= (param->getS()*param->getB()*tmax[itev]-us_v[itev])) && ( (*itapp)->getP() <= (P-us_p[itev]) ))
	{
	 
	  us_v[itev]+=(*itapp)->getV()*(*itapp)->getN();
	  ev[itev].push_back(*itapp);
	  us_p[itev]+=(*itapp)->getP();
	  tmax[itev]=std::max(tmax[itev],(*itapp)->getEta());
	  break;
	}
      ++itev;
    }
    if( itev==ev.size() )
      {

	us_v.push_back((*itapp)->getV()*((*itapp)->getN()));
	us_p.push_back((*itapp)->getP());
	tmax.push_back((*itapp)->getEta());
	std::vector<App*> nev;
	nev.clear();
	nev.push_back(*itapp);
	ev.push_back(nev);

      }

    itapp=applist->erase(itapp); 
  }


   packs_to_list(ev,applist);

   return 0;
  
}

//----------------------------------------------------------------------------------------------------------------

int pack_wide(std::vector<App*> *applist,Parameters *param){
  //std::cout<<"Wide pack scheduling"<<std::endl;
  std::vector< std::vector<App*> > ev; 
  std::vector<long double> us_v ;
  std::vector<int> us_p ;
  std::vector<long double> tmax; 
  
  us_v.clear();
  us_p.clear();
  tmax.clear();
  
  switch ( param->getSchedPol() ){
  case 1:
    std::sort(applist->begin(),applist->end(),CompTime);
    break;
  case 3:
    std::sort(applist->begin(),applist->end(),CompTChar);
    break;
  case 7:
    std::sort(applist->begin(),applist->end(),CompRel);
    break;
  }
  
  //sort applications according to sorting policy for packs building?

  unsigned int itev=0;
  while (!applist->empty()){

    
    while(itev != ev.size() ) {
      auto itapp = applist->begin();
      while (itapp != applist->begin()) {// while pour suppress
	
	if ( (((*itapp)->getV()*(*itapp)->getN()) <= (param->getS()*param->getB()*tmax[itev]-us_v[itev])) && ( (*itapp)->getP() <= (P-us_p[itev]) )) //to define
	  {
	    us_v[itev]+=(*itapp)->getV()*(*itapp)->getN();
	    ev[itev].push_back(*itapp);
	    us_p[itev]+=(*itapp)->getP();
	    tmax[itev]=std::max(tmax[itev],(*itapp)->getEta());
	    itapp=applist->erase(itapp);
  	  }
	else
	  ++itapp;
      }
      ++itev;
    }
    if( itev==ev.size() )
	{
	  us_v.push_back((*applist->begin())->getV()*((*applist->begin())->getN()));
	  us_p.push_back((*applist->begin())->getP());
	  tmax.push_back((*applist->begin())->getEta());
	  std::vector<App*> nev;
	  nev.clear();
	  nev.push_back(*applist->begin());
	  ev.push_back(nev);
	  applist->erase(applist->begin());
	}    
  } // End applist empty
  //std::cout<<"Packs are over"<<std::endl;
  return packs_to_list(ev,applist);
}
    
//----------------------------------------------------------------------------------------------------------------

  int scheduler(std::vector<App*> *applist, Parameters *param){
    switch ( param->getSchedPol() ) {
  case 0 :
    std::cerr<<"pack_depth 0\n";
    pack_depth(applist,param);
    break;
  case 1:
    std::cerr<<"pack_wide 1\n";
    pack_wide(applist, param);
    break;
  case 2 :
    std::cerr<<"pack_depth 2\n";
    pack_depth(applist, param);
    break;
  case 3:
    std::cerr<<"pack_wide 3\n";
    pack_wide(applist,param);
    break;
    case 6:
      pack_depth(applist,param);
      break;
    case 7:
      pack_wide(applist,param);
      break;
  default :
    std::cerr<<"LS\n";
    //std::cout<<"List scheduling"<<std::endl;
    //applist=list_scheduling(applist,scheduler_pol,controller_pol);
    break;
    }
    std::cerr<<"exiting scheduler"<<std::endl;
    return 0;
  }
