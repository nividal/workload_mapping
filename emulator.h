#ifndef EMULATOR_H
#define EMULATOR_H

#include "Platform.h"
#include "Parameters.h"

//long double next_exclusive();
int resolve(Platform *platform, Parameters *param,  long double next_event);

#endif
