#ifndef SCHEDULER_H // include guard
#define SCHEDULER_H



#include<vector>
#include <algorithm>    // std::sort



#include "App.h"
#include "Parameters.h"

/*
#ifndef B
#define B 1.0
#endif
*/

int packs_to_list(std::vector< std::vector<App*> > ev, std::vector<App*> *applist);
int pack_depth(std::vector<App*> *applist,Parameters *param);
int pack_wide(std::vector<App*> *applist,Parameters *param);
int scheduler(std::vector<App*> *applist, Parameters *param);

#endif
