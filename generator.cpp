#include "generator.h"
#include "Platform.h" //debug print_vector

const long double PI = 3.141592653589793238463;
long double l[11];




long double f_dec(long double n){
return round(n*1000000)/1000000;
}

//---------------------------------------------------------------
  //set up random generators
std::random_device rd{};
std::mt19937 gen{rd()};

std::default_random_engine gen_random;
std::uniform_real_distribution<long double> distribution(0.0,1.0);


//---------------------------------------------------------------
//Tools for generation according to alpha


//# Truncated normal tools
long double truncatedNormal(long double mean, long double sigma, long double lower, long double upper){
  std::normal_distribution<long double> Z(0.0,1.0);
  long double X = mean + sigma*Z(gen);
  if (X >= lower && X <= upper)
    return X;
  return truncatedNormal(mean, sigma, lower, upper);
}

long double density(long double x){
  return 1/(sqrt(2*PI))*exp(-1/2*(x*x));
}

long double cumulative(long double x){
  return (1+erf(x/sqrt(2)))/2.0;
}

//Mean of a truncated normal(mu,sigma) |  a<X<b
long double mean_trunc(long double mu,long double sigma, long double a, long double b){
  long double alpha=(a-mu)/sigma;
  long double beta=(b-mu)/sigma;
  
  return mu+sigma*((density(alpha)-density(beta))/(cumulative(beta)-cumulative(alpha)));
}


//
int rec_li(int low,int high,long double tgt){
  long double lam;
  if (high<=low)
    return 0;
  if (high==low+1)
    {
      lam=(pow(2,high)-tgt)/(pow(2,high)-pow(2,low));
      l[low]*=lam;
      l[high]*=(1-lam);
      return 0;
    }
  int it=low;
  while(pow(2,it)<tgt)
    it++;
  long double t1=distribution(gen_random)*(pow(2,it-1)-pow(2,low))+pow(2,low);
  long double t2=distribution(gen_random)*(pow(2,high)-pow(2,it))+pow(2,it);
  
  rec_li(low,it-1,t1);
  rec_li(it,high,t2);

  lam=(t2-tgt)/(t2-t1);
  for (int i=low;i<it;i++)
    l[i]*=lam;
  for (int j=it;j<high+1;j++)
    l[j]*=(1-lam);
  return 0;
}

int compute_li(long double alpha,long double mean_p){
  for (int it=0;it<11;it++)
    l[it]=1;

  
  rec_li(0,10,mean_p);
  /*
  long double test=0;
  for (int jt=0;jt<12;jt++){
    std::cout<<"l["<<jt<<"]="<<l[jt]<<std::endl;
    test+=l[jt];
  }
  std::cout<<"tot:"<<test<<std::endl;
  */
  return 0;
}

int generate_pi(){
  long double p=distribution(gen_random);
  int it=0;
  
  while(p>l[it] && it < 10){
    p-=l[it];
    it++;
  }
  //std::cout<<"it :"<<it<<" pow(2,it):"<<pow(2,it)<<std::endl;
  return pow(2,it);  
}


// Input generation functions
std::vector<App*> default_gen(){
  //TO DEFINE
  std::vector<App*> list;
  return list;
}


long double verif_alpha(std::vector<App*> applist){
  long double vi=0;
  long double piwivi=0;

  for (auto it =applist.begin(); it != applist.end() ; it++){
    vi+= ( (*it)->getV() );
    piwivi+=  ((long double)(*it)->getP()) * ( (*it)->getV() + (*it)->getW() );
  }
  return (P*vi)/(piwivi);
}
/*
long double verif_mu(std::vector<App*> applist){
  long double v,w;
  for (auto it=applist.begin();it != applist.end();it++){
    v+=(*it)->getV();
    w+=(*it)->getW();
  }
  return v/w;
}
*/
std::vector<App*> alpha_gen(long double alpha){
  int n_app=100+(rand()%400);
  
  // mean, varianc
  //std::cout<<"expected mean1 & mean2 0.289437636661119 0.710562363338881"<<std::endl;
  long double mean1=mean_trunc(0.1,0.1,0,1);
  long double mean2=mean_trunc(0.9,0.1,0,1);
  //std::cout<<"get values "<<mean1<<" "<<mean2<<std::endl;
  long double beta=distribution(gen_random);

  long double mu_t=beta*mean1+(1-beta)*mean2;
  long double mean_p=(mu_t*P)/((mu_t+1)*alpha);
  
  
  long double mu=(alpha*mean_p)/(P-alpha*mean_p);
  long double gamma=(mean2-mu)/(mean2-mean1);

  compute_li(alpha, mean_p);

 
  std::vector<App*> applist;
  applist.clear();
  long double w;
  int p;
  int n;
   
  long double g;
  long double v;
  for (int id=0;id<n_app;id++){
    w = 10 + distribution(gen_random)*(100-10); 
    p=generate_pi();
    n=100+rand()%(1000-100);

    
    g=distribution(gen_random); 
    if (g<gamma)
      v=truncatedNormal(0.1,0.1, 0, 1)*w;
    else
    v=truncatedNormal(0.9,0.1,0,1)*w;
    

    /*
    gamma=truncatedNormal(alpha,0.1,0,1);
    v=w*gamma;
    w=(1-gamma)*w;
    */

    v=v*p/P;
    v=std::max(v,(long double)0.0001);
    applist.push_back((new App(id,w,p,n,v)));


    
    
  }
  return applist;
}

/*
  Uniform generation
  
 */

std::vector<App*> uni_gen(long double alpha){
  int n_app=100+(rand()%400);
  
  // mean, varianc
  //std::cout<<"expected mean1 & mean2 0.289437636661119 0.710562363338881"<<std::endl;
  long double mean1=mean_trunc(0.1,0.1,0,1);
  long double mean2=mean_trunc(0.9,0.1,0,1);
  //std::cout<<"get values "<<mean1<<" "<<mean2<<std::endl;
  long double bound=( alpha/(1-alpha) - mean2  )/(mean1-mean2)   ;
  long double beta=distribution(gen_random) * (1-bound) + bound;
  
  long double mu_t=beta*mean1+(1-beta)*mean2;
  long double mean_p=(mu_t*P)/((mu_t+1)*alpha);
  
  long double mu=(alpha*mean_p)/(P-alpha*mean_p);
  long double gamma=(mean2-mu)/(mean2-mean1);

  //Processor distribution

  long double mp=floor(log(mean_p)/log(2));
  long double low = 1/mp * (pow(2,mp+1)-1);
  long double high = 1 /(11-mp) * (pow(2,12)-pow(2,mp+1));
  long double x=(high-mean_p)/(high-low);
  
  
  std::vector<App*> applist;
  applist.clear();
  long double w;
  int p;
  int n;
   
  long double g;
  long double v;
  long double temp;
  for (int id=0;id<n_app;id++){
    w = 10 + distribution(gen_random)*(100-10);

    temp=distribution(gen_random);
    
    if (temp<x){
      temp=floor(distribution(gen_random)*(mp));
      p=pow(2,temp);
    }
    else{
      temp=floor(distribution(gen_random)*(10-mp)+mp);
      p=pow(2,temp);
    }
    
    n=100+rand()%(1000-100);

    

    /*
    g=distribution(gen_random); 
    if (g<gamma)
      v=truncatedNormal(0.1,0.1, 0, 1)*w;
    else
    v=truncatedNormal(0.9,0.1,0,1)*w;
    */

    
    gamma=truncatedNormal(alpha,0.1,0,1);
    v=w*gamma;
    w=(1-gamma)*w;
    

    v=v*p/P;
    v=std::max(v,(long double)0.0001);
    applist.push_back((new App(id,w,p,n,v)));

    
  }
  return applist;
}






/*
std::vector<App*> alpha_gen_2(long double alpha){
  std::vector<App*> applist;
  applist.clear();
  long double w;
  int p;
  int n;
   
  long double g;
  long double v;
  
  
  long double sum_v=0;
  long double pwv=0;
  for (int id=0;id<500;id++){
    w = 10 + distribution(gen_random)*(100-10); 
    p=generate_pi();
    n=5000+rand()%(100000-5000);
    
    g=distribution(gen_random);
    
    if (g<0.5)
      v=truncatedNormal(0.1,0.1, 0, 1)*w;
    else
      v=truncatedNormal(0.9,0.1,0,1)*w;
    //std::cout<<"p: "<<p<<" v: "<<v<<std::endl;
    applist.push_back((new App(id,w,p,n,v)));
    sum_v+=v;
    pwv+=(p*(w+v));
  }

  int id=500;
  //std::cout<<"Test1:"<<std::abs( (P*sum_v/pwv) - alpha)<<" vs "<<alpha<<std::endl;
  while ( std::abs( (P*sum_v/pwv) - alpha) < 0.01 ){
    //std::cout<<"Test2:"<<std::abs( (P*sum_v/pwv) - alpha)<<" vs "<<alpha<<std::endl;
    w = 10 + distribution(gen_random)*(100-10); 
    p=generate_pi();
    n=5000+rand()%(100000-5000);
    
    g=distribution(gen_random);
    
    if (P*v/pwv < alpha)
      v=truncatedNormal(0.1,0.1, 0, 1)*w;
    else
      v=truncatedNormal(0.9,0.1,0,1)*w;
    applist.push_back((new App(id,w,p,n,v)));
    sum_v+=v;
    pwv+=(p*(w+v));
    id++;
  }
   std::cout<<"VERIF ALPHA:"<<verif_alpha(applist)<<" / " <<alpha<<std::endl;

  
  return applist;
  
}
*/

std::vector<App*> mira_gen(double alpha){
  int n_app=400+(rand()%1600);
  double eta;
  int p;
  
  double mu_n;
  double sigma_n;
  double beta;
  std::vector<App*> applist;
  applist.clear();
  long double w;
  int n;
  long double v;
  //Processors disponibles -> P = 49152
  // p suivant une loi exponentielle de paramètre 0.001353803
  std::default_random_engine generator;
  std::exponential_distribution<double> pi_distrib(0.001353803);
  for (int id=0;id<n_app;id++){
  	eta=-1;
 	p=pi_distrib(generator);

/*
  //If we want to remove "big" applications
  while (p>8192)
    p=pi_distrib(generator);
*/

 	if (p<=512)
 		p=512;
 	if ((512<p)&&(p<=1024))
 		p=1024;
 	if ((1024<p)&&(p<=2048))
 		p=2048;
 	if ((2048<p)&&(p<=4096))
 		p=4096;
 	if ((4096<p)&&(p<=8192))
 		p=8192;
 	if ((8192<p)&&(p<=16384))
 		p=16384;
 	if ((16384<p)&&(p<=24576))
 		p=24576;
 	if ((24576<p)&&(p<=32768))
 		p=32768;
 	if (32768<p)
 		p=49152;
  
  while(eta<0){
	  if (p < 4096){
	    mu_n=3600;
	    sigma_n = 0.1*mu_n; //to discuss 
	    std::normal_distribution<double> norm_distrib(mu_n,sigma_n); 
	    eta=norm_distrib(generator);
	 	}

	  if ((p>= 4096)  && (p < 16384)){
	    mu_n=7200;
	    sigma_n = 0.1*mu_n; //to discuss 
	    std::normal_distribution<double> norm_distrib(mu_n,sigma_n);
	    eta=norm_distrib(generator);
	    }
	  
    if (p >= 16384){
	    mu_n=1800;
	    sigma_n = 0.1*mu_n; //to discuss 
	    std::normal_distribution<double> norm_distrib(mu_n,sigma_n); 
	    eta=norm_distrib(generator);
	    }
     
}
//truncated normal de moyenne alpha
  //beta=truncatedNormal(alpha,0.1,0,1);
  
  
  beta=distribution(gen_random);
  if (beta <= alpha)
    beta=truncatedNormal(0.1,0.1,0,1);
  else
    beta=truncatedNormal(0.9,0.1,0,1);
  
  n=distribution(gen_random)*(100-10)+10;
  w=std::max(f_dec((1-beta)*eta/n),(long double)0.001);
  v=std::max(f_dec(beta*eta/n*p/P),(long double)0.001);
  applist.push_back((new App(id,w,p,n,v)));
  
  }
  //Temps moyen = 1h pour <4k, 2h si 4k< <16k, 30 min  >16k en normale tronquée ? (en t calcul complet, ensuite on divisera par n_it, et on découpera en I/O et en calcul
  
  //Pourcentage d'I/O = (2-50% en uniforme)
  //Nombre d'iteration ?
  return applist;
}


int generator(Parameters param){
  std::cout<<"Starting generator"<<std::endl;
  std::vector<App*> applist;
  int n_inputs=param.getInputs();
  std::string outputfile;
  int opt=param.getGen();
  //Input generation according to parameters
  
  for (int n_in=0;n_in<n_inputs;++n_in){
    outputfile="workloads/";
    //inputs set outputfiles "workloads/bla

    
    if (opt==0){
      outputfile+=std::to_string(n_in)+".dat"; 
      applist=default_gen();
    }
      
    if (opt==1){
      std::vector<long double> alpha_tab(param.getAlpha());
      for (auto al_it=alpha_tab.begin();al_it!= alpha_tab.end();al_it++){
	std::cout<<"GENERATING INPUT ALPHA "<<*al_it<<std::endl;
	outputfile+="alpha_"+std::to_string(*al_it).substr(0,4)+"_n"+std::to_string(n_in)+".dat"; 
	applist=alpha_gen(*al_it);
	
	//std::cout<<"verif 1 ;target alpha:"<<*al_it<<"   -> result is "<<verif_alpha(applist)<<std::endl;
	
	//Print into files
	std::ofstream output(outputfile);
	long double den=0;
	long double num=0;
	if (output.is_open()){
	  for (std::vector<App*>::iterator ait=applist.begin();ait != applist.end();ait++){
	    std::cout<<std::distance(ait,applist.begin())<<" "<<std::distance(ait,applist.end())<<std::endl;
	    output<<"jacobi:5000:"<<(*ait)->getW();
	    output<<":"<<(*ait)->getP();
	    output<<":1:"<<(*ait)->getV();
	    output<<":"<<(*ait)->getN()<<std::endl;

	    num+=(*ait)->getV();
	    den+=(*ait)->getP()*( (*ait)->getV() + (*ait)->getW()  );
	    
	    std::cout<<"jacobi:5000:"<<(*ait)->getW();
	    std::cout<<":"<<(*ait)->getP();
	    std::cout<<":1:"<<(*ait)->getV();
	    std::cout<<":"<<(*ait)->getN()<<std::endl;
	    
	  }
	  output.close(); 
	}
	else std::cout <<"Unable to write input file "<<outputfile<<" ; open error\n";    
	outputfile="workloads/";
	std::cout<<"workload "<<n_in+1<<" and alpha:"<<*al_it<<"completed"<<std::endl;
      }
      std::cout<<n_in+1<<" input set have already been generated over "<<n_inputs<<std::endl;
    }


    if (opt==2){
      std::vector<long double> alpha_tab(param.getAlpha());

      for (auto al_it=alpha_tab.begin();al_it!= alpha_tab.end();al_it++){ 
       	outputfile+="mira_"+std::to_string(*al_it).substr(0,4)+"_n"+std::to_string(n_in)+".dat"; 
	      applist=mira_gen(*al_it);

	//Print into files
	     std::ofstream output(outputfile);
	     long double den=0;
	     long double num=0;
	     if (output.is_open()){
	       for (std::vector<App*>::iterator ait=applist.begin();ait != applist.end();++ait){
	         output<<"jacobi:5000:"<<(*ait)->getW();
	         output<<":"<<(*ait)->getP();
	         output<<":1:"<<(*ait)->getV();
	         output<<":"<<(*ait)->getN()<<std::endl;

	         num+=(*ait)->getV();
	         den+=(*ait)->getP()*( (*ait)->getV() + (*ait)->getW()  );
	         }
	       output.close();
	       }
	     else std::cout <<"Unable to write input file "<<outputfile<<" ; open error\n";    
	   outputfile="workloads/";
      }
     
    }


    if (opt==3){
      std::vector<long double> alpha_tab(param.getAlpha());
      for (auto al_it=alpha_tab.begin();al_it!= alpha_tab.end();al_it++){
	std::cout<<"GENERATING INPUT ALPHA "<<*al_it<<std::endl;
	outputfile+="alpha_"+std::to_string(*al_it).substr(0,4)+"_n"+std::to_string(n_in)+".dat"; 
	applist=uni_gen(*al_it);
	
	//std::cout<<"verif 1 ;target alpha:"<<*al_it<<"   -> result is "<<verif_alpha(applist)<<std::endl;
	
	//Print into files
	std::ofstream output(outputfile);
	long double den=0;
	long double num=0;

	
	if (output.is_open()){
	  for (std::vector<App*>::iterator ait=applist.begin();ait != applist.end();ait++){
	    std::cout<<std::distance(ait,applist.begin())<<" "<<std::distance(ait,applist.end())<<std::endl;
	    output<<"jacobi:5000:"<<(*ait)->getW();
	    output<<":"<<(*ait)->getP();
	    output<<":1:"<<(*ait)->getV();
	    output<<":"<<(*ait)->getN()<<std::endl;

	    num+=(*ait)->getV();
	    den+=(*ait)->getP()*( (*ait)->getV() + (*ait)->getW()  );
	    
	    std::cout<<"jacobi:5000:"<<(*ait)->getW();
	    std::cout<<":"<<(*ait)->getP();
	    std::cout<<":1:"<<(*ait)->getV();
	    std::cout<<":"<<(*ait)->getN()<<std::endl;
	    
	  }
	  output.close(); 
	}
	else std::cout <<"Unable to write input file "<<outputfile<<" ; open error\n";    
	outputfile="workloads/";
	std::cout<<"workload "<<n_in+1<<" and alpha:"<<*al_it<<"completed"<<std::endl;
      }
      std::cout<<n_in+1<<" input set have already been generated over "<<n_inputs<<std::endl;
    }



    
  }
  std::cerr<<"Generation completed"<<std::endl;
  return 0;  
}

